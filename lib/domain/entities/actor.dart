class Actor {
  final int id;
  final String name;
  final String profilePath;
  final String? charcater;

  Actor({
    required this.id,
    required this.name,
    required this.profilePath,
    required this.charcater,
  });
}
